import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailComponent } from './detail.component';
import { HomeRoutingModule } from './detail-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from '../services/http.service';
import { MaterialModule } from '../material.module';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    MaterialModule
  ],
  exports: [
    DetailComponent
  ],
  declarations: [
    DetailComponent
  ],
  providers: [
      HttpService
  ],
})
export class DetailModule { }