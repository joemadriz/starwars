import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  constructor(
    private httpService: HttpService,
    private router: Router 
  ) { }

  public urlDetail: string = '';
  public resultDetail: any | '';
  public peopleIndex: number = 0;
  public name: string | undefined;
  public birthDate: string | undefined;
  public eyeColor: string | undefined;
  public hairColor: string | undefined;
  public skinColor: string | undefined;
  public height: string | undefined;
  public mass: string | undefined;
  public films: any = [];
  public homeWorld: any;
  public homeWorldName: string | undefined;
  public homeWorldPopulation: string | undefined;
  public homeWorldOrbit: string | undefined;
  public homeWorldRotation: string | undefined;
  public isLoading: boolean = true;

  ngOnInit(): void {
    let totalLength = this.router.url.length;
    let id = this.router.url.slice(8, totalLength);
    console.log('id: ', id)
    this.httpService.getDetail(id).subscribe(data => {
      this.isLoading = false;
      this.resultDetail = data;
      this.name = this.resultDetail.name;
      this.birthDate = this.resultDetail.birth_year;
      this.eyeColor = this.resultDetail.eye_color;
      this.hairColor = this.resultDetail.hair_color;
      this.skinColor = this.resultDetail.skin_color;
      this.height = this.resultDetail.height;
      this.mass = this.resultDetail.mass;

      this.getFilms();
      this.getHomeWorld();
    });
    
  }
  getFilms() {
    this.resultDetail.films.forEach((item: any) => {
      this.httpService.getFilms(item).subscribe(data => {
        this.films.push(data);
      });
    });
  }

  getHomeWorld() {
    let url = this.resultDetail.homeworld;
    this.httpService.getHomeWorld(url).subscribe(data => {
      this.homeWorld = data;
      this.homeWorldName = this.homeWorld.name;
      this.homeWorldOrbit = this.homeWorld.orbital_period;
      this.homeWorldRotation = this.homeWorld.rotation_period;
      this.homeWorldPopulation = this.homeWorld.population;
    });
  }
  getBack() {
    this.router.navigate(['/home']);
  }

}
