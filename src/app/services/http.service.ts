import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
    constructor(
        private http: HttpClient,
    ) { }

    public peopleUrl: string = 'https://swapi.dev/api/people'; 
    public resultPeople: any;

    public getPeople(pageIndex?:number): Observable<any> {  
        return (pageIndex ? this.http.get(this.peopleUrl + `/?page=${pageIndex}`) : this.http.get(this.peopleUrl));
    }
    public getDetail(id:string): Observable<any> {
        return this.http.get(this.peopleUrl + `/${id}`);
    }
    public getFilms(film:string): Observable<any> {
        return this.http.get(film);
    }
    public getHomeWorld(url:string): Observable<any> {
        return this.http.get(url);
    }
    public getStarShips(url:string): Observable<any> {
        return this.http.get(url);
    }
    public getVehicles(url:string): Observable<any> {
        return this.http.get(url);
    }
}