import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  constructor(
    private httpService: HttpService,
    private router: Router,
    // MatPaginator Output
    // public pageEvent: PageEvent
  ) { }

  public peopleResult: any;
  public people: any;
  // MatPaginator Inputs
  public length: number = 0;
  public pageSize: number = 10;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public pageIndex: number = 0;
  public isLoading: boolean = true;
  public indexPeople: any;  
  public errorMessage: any;  
  public isError: boolean = false;
  

  ngOnInit(): void {
    this.isError = false;
    this.getPeople();
  }

  asignValues() {
    this.length = this.peopleResult.count;
    this.people = this.peopleResult.results;
    this.people.forEach((element:any, index:number) => {
      let totalLength = element.url.length;
      this.indexPeople = element.url.slice(29, totalLength);
      this.indexPeople = this.indexPeople.replace(/\//, '');
      element.index = this.indexPeople;
      console.log('INDEX: ', this.indexPeople)
    });
  }
  getPeople(pageId?: string) {
    this.httpService.getPeople(this.pageIndex).subscribe({
      next: (data) => {
        this.isError = false;
        this.isLoading = false;
        this.peopleResult = data;
        this.scrollUp();
        this.asignValues();
      },
      error: (error) => { 
        this.isError = true;
        this.isLoading = false;
        this.errorMessage = {
          error : error.error.detail,
          message : error.message
        }
        console.log(this.errorMessage.error + ' ' + this.errorMessage.message);
      }
    });
  }
  getDetail(index:any) {
    let indexNumber = Number(index);
    this.router.navigate(['/detail', indexNumber]);
    this.scrollUp();
  }
  changePage(event:any) {
    this.isLoading = true;
    this.pageIndex = event.pageIndex === 0 ? event.pageIndex = undefined : event.pageIndex + 1;
    this.getPeople();
    console.log('pageIndex paginator: ',event.pageIndex); 
  }
  scrollUp() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }
  reloadComponent(){
    this.isLoading = true;
    this.getPeople();
  }
}
